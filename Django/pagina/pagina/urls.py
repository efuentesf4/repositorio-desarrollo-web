
from django.contrib import admin
from django.urls import path
from pagina.vista import index


urlpatterns = [
    path('admin/', admin.site.urls),
    path('index/' , index), 
]